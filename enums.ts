export enum Role {
  Teacher = 'TEACHER',
  Guard = "Guard",
  Student= 'STUDENT',
  Librarian = "LIBRARIAN",
  Cleaner = 'CLEANER'
};

export enum Subject {
  Biology =  "BIOLOGY",
  Math = "MATH",
  History = "HISTORY",
  English = "ENGLISH",
  Law = 'LAW',
  Psychology = 'PSYCHOLOGY',
  Pedagogy = "PEDAGOGY"
}

export enum Textbook {
  BiologyTextbook =  "Life Sciences: Exploring the Living World",
  MathTextbook = "Mathematics: A Journey Through Numbers",
  HistoryTextbook = "The Chronicles of Humanity: A Historical Perspective",
  EnglishTextbook = "Language and Literature: Mastering English Skills",
  LawTextbook = 'Legal Studies: Understanding the Legal System',
  PsychologyTextbook = 'The Human Mind: Exploring Psychological Principles',
  PedagogyTextbook = "Teaching and Learning: Strategies for Educators"
}

export{}



