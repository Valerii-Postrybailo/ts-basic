import { Role } from "./enums";
import { IPerson  } from "./interfaces";
import { UniversityWorkersBase } from "./workersBase";
import { StudentColumbineer, Student } from "./students";

class Guard extends UniversityWorkersBase {
  constructor(name: string, age: number) {
    super(name, age, Role.Guard);
  }

  calculateSalary(): number {
    return 1000;
  }

  checkDocuments():string{
    return "-You doesn't look like student, can I check you document, pls?"
  }

  checkSuspicion(strangeThing: [string, string, string] | null): string {

    if (strangeThing === null) {
      return "Sorry for the inconvenience, please come through..."


    } else if (typeof strangeThing === "object") {
      return "He has a weapon!"
    }

    return "Wtf..."
  }
}

const guard1 = new Guard("Vasilii", 34)

const studentC1 = new StudentColumbineer("John", 18, "freshman", true);
const guitarCase= studentC1.takeGuitarCase(true);
console.log(guitarCase)
console.log("Columbineer guitar case if he has bad mood:", guitarCase);

console.log(guard1.checkSuspicion(guitarCase))


function isStudent(person: IPerson | Student | StudentColumbineer): person is Student | StudentColumbineer {
  return (person as Student | StudentColumbineer).lvl === undefined;
}

const nonamePerson: IPerson = { name: 'NoName', age: 50 };

if (isStudent(nonamePerson)){
  console.log(guard1.checkDocuments())
} else {
  console.log("It was a student and he calmly passed the guard")
}

export{}
