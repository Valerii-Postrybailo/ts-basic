import { Subject, Textbook} from "./enums";

export interface IPerson {
  name: string;
  age: number;
};

export interface IUniversitySubject  {
  id: number;
  name: Subject;
  isCommon : Boolean;
}

export interface Book {
  id: number;
  subject:Subject,
  name:Textbook,
}

export{}
