import { Role} from "./enums";
import { Book } from "./interfaces";

export abstract class UniversityWorkersBase {
    private _salary: number;

    constructor(public name: string, public age: number, public role: Role) {
        this._salary = 0;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }

    abstract calculateSalary(): number;
}

export class Librarian extends UniversityWorkersBase {
    private _books: Book[] = [];

    constructor(name: string, age: number)  {
        super(name, age,  Role.Librarian);
    }

    calculateSalary(): number {
        return 2000;
    }

    addBook(complaint: Book): void {
        this._books.push(complaint);
        console.log(`Book added to the list of debts: ${JSON.stringify(this._books)}`);
    }

    handleBookDebt(bookId: number): void {
        this._books = this._books.filter((book) => book.id !== bookId);
        console.log(`Book with id ${bookId} removed from the list of debts.`);
    }
}

@logClass
class Cleaner extends UniversityWorkersBase  {

    @logProperty
    private _personalMop: string;

    constructor(name: string, age: number) {
        super(name, age, Role.Cleaner);
        this._personalMop = "simple mop";
    }

    @logGetter
    get takeMop(): string {
        return this._personalMop;
    }

    @logSetter
    set createPersonalMop(value: string) {
        this._personalMop = value;
    }

    @logSalary
    calculateSalary(): number {
        return 2000;
    }

}



function logClass(target: Function) {
    console.log(`Class name: ${target.name}`);
}

function logProperty(target: any, propertyKey: string) {
    console.log(`Property: ${propertyKey}`);
}


function logGetter(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalGetter = descriptor.get;

    descriptor.get = function (this: PropertyDescriptor) {
        console.log(`Getting ${propertyKey}`);
        return originalGetter!.call(this);
    };

    return descriptor;
}

function logSetter(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    console.log(`Setter: ${propertyKey}`);
}

function logSalary(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function(...args: any[]) {
        const startTime = Date.now();
        const argsString = JSON.stringify(args);

        try {
        console.log(`Calling method: ${propertyKey}`);
        console.log(`Arguments: ${argsString}`);

        const result = originalMethod.apply(this, args);

        console.log(`Method ${propertyKey} executed successfully`);
        console.log(`Execution time: ${Date.now() - startTime}ms`);

        return result;
    } catch (error) {
        console.log(`An error occurred in method ${propertyKey}`);
        console.log(`Error message: ${error}`);
        throw error;
        }
    };

    return descriptor;
    }

const cleaner1 = new Cleaner("Natasha", 45);

console.log(cleaner1);
console.log("Salary:",cleaner1.calculateSalary());
console.log(cleaner1.takeMop);

cleaner1.createPersonalMop = "a mop with a phoenix feather";

console.log(cleaner1.takeMop);

export{}
