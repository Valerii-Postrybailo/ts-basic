import { Role} from "./enums";
import { IPerson } from "./interfaces";

abstract class StudentBase {
  private _scholarship: number;
  private _marks: number[];

  protected _iq: number;

  constructor(
    public name: string, public age: number, 
    public lvl: 'freshman' | 'sophomore' | "gradeJunior"| "senior",
    public role: Role, public privilege:boolean) {
      this._scholarship = 0;
      this._marks =[];
      this._iq =0;
  }

  get marks():number[]{
    return this._marks;
  }

  set marks(value:number[]){
    this._marks = [...value];
  }

  get iq():number{
    return this._iq;
  }

  set iq(value:number){
    this._iq = value;
  }

  get scholarship(): number {
      return this._scholarship;;
  }

  set scholarship(value: number) {
      this._scholarship = value;
  }

  abstract calculateScholarship(): number;
  abstract calculateAverageMark(): number;
}


export class Student extends StudentBase {
  constructor(
    name: string, age: number, 
    lvl:'freshman' | 'sophomore' | "gradeJunior"| "senior",
    privilege?:boolean) {
      super(name, age, lvl, Role.Student, privilege  || false);
  }

  calculateAverageMark(): number {
    return this.marks.reduce( (a,b) => a+ b ) / this.marks.length;
  }

  calculateScholarship(): number {
    const averageMark = this.calculateAverageMark();
    
    if (this.privilege || averageMark >3){
      return 3000;
    } 

    return 0;
  }
}

interface SpecialStudent extends IPerson {
  lvl:'freshman' | 'sophomore' | "gradeJunior"| "senior",
  genius: number
}

const student1 = new Student('John', 30, "senior", true);
const student2 = new Student('Teo', 23, "senior", true);

const SpecialStudent: SpecialStudent = {
  name: "Rafik",
  age:23,
  lvl:"freshman",
  genius: 1000,
} as SpecialStudent;


console.log("SpecialStudent:",SpecialStudent);

console.log("basik student:",student1);
console.log("Student marks before studying year:", student1.marks);
student1.iq = 130;
student1.marks = [3,3,3];
console.log("Student marks after studying year:", student1.marks);
console.log("Student iq:",student1.iq);
console.log("Student 2 iq:",student2.iq);

console.log("Average mark", student1.calculateAverageMark());
console.log("Scholarship", student1.calculateScholarship());

export class StudentColumbineer extends StudentBase {

  @addProperty
  hasFriend: boolean;

  constructor(
    name: string, age: number, 
    lvl:'freshman' | 'sophomore' | "gradeJunior"| "senior",
    privilege?:boolean) {
      super(name, age, lvl, Role.Student, privilege  || false);
      this.hasFriend = false;
  }

  calculateAverageMark(): number {
    
    return this.marks.reduce( (a,b) => a+ b ) / this.marks.length;
  }

  calculateScholarship(): number {
    const averageMark = this.calculateAverageMark();
    
    if (this.privilege || averageMark >3){
      return 3000;
    } 

    return 0;
  }

  @addFirstInterestingMethod
  takeGuitarCase(badMood: boolean): [string, string, string] | null {
    return null
  }

  @addSecondInterestingMethod
  informFriends(hasFriend: boolean) {}

}


function addFirstInterestingMethod(target: any, methodName: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    const [badMood] = args;
    if (badMood) {
      return ["shootgun", "uzi", "grenades"];
    } else {
      return null;
    }
  };

  return descriptor;
}

const studentC1 = new StudentColumbineer("John", 18, "freshman", true);
const weapons = studentC1.takeGuitarCase(true);
console.log("Columbineer guitar case if he has bad mood:", weapons); 

const studentC2 = new StudentColumbineer("Alice", 19, "sophomore", false);
const weapons2 = studentC2.takeGuitarCase(false);
console.log("Columbineer guitar case if he has good mood:",weapons2); 

function addProperty(target: any, propertyName: string) {
  Object.defineProperty(target, propertyName, {
    value: true,
    writable: true,
    enumerable: true,
    configurable: true,
  });
}

function addSecondInterestingMethod(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

  descriptor.value = function (hasFriend: boolean) {
    if (hasFriend) {
      return "The shooter warned his friends not to go to school";
    }
    return "any friends...";
  };

  return descriptor;
}

const studentC3 = new StudentColumbineer("John", 17, "senior", false);

console.log("Columbineer", studentC3)
const hasFriend = true;
const result = studentC3.informFriends(hasFriend);
console.log(result);

export{}
