import { Role, Subject, Textbook} from "./enums";
import { Book, IPerson } from "./interfaces";
import { UniversityWorkersBase, Librarian } from "./workersBase";

type PartOfUniversityCommunity = IPerson & { role: Role;} & { salary: number; } | { scholarship: number; } & {books? :[]};

type ReadonlyBook = Readonly<Book>;

interface IReader extends IPerson {
    readerTicket: ReaderTicket;
}

class ReaderTicket {
    private _items: Book[] = [];

    get items(): ReadonlyBook[] {
        return this._items;
    }

    addItem(item: Book): void {
        this._items.push(item);
    }

    removeItem(id: number): void {
        this._items = this._items.filter((item) => item.id !== id);
    }

    clear(): void {
        this._items = [];
    }
}

class Reader implements IReader {
    constructor(public name: string, public gender:string, public age: number, public readerTicket: ReaderTicket) {};
}


class Teacher extends UniversityWorkersBase {
  constructor(name: string, age: number) {
    super(name, age, Role.Teacher);
  }

  calculateSalary(): number {
    return 4500;
  }

  isItBookwarm(reader: Reader, university: Univeristy<Book>): string | undefined {
    const booksInTicket = reader.readerTicket.items;

    if (booksInTicket.length > 5) {
      if (university.name === "VDPU"){
        return "Not bad, nice result for this university, 5+";
      }
    } else {
        return '';
    }
}
};

class Univeristy<T extends Book> {

  private _universityWorkers: PartOfUniversityCommunity[] = [];
  private _books: T[] = [];

  constructor(public name: string) {}

  showName():string{
    return `University name: ${this.name}`;
  }

  @Log('Worker added')
  addEmployee(_universityWorker: PartOfUniversityCommunity): void {
    this._universityWorkers.push(_universityWorker);
  }

  @Log("Uni workers")
  showUniWorkers(){
    return this._universityWorkers;
  }

  showBooks():T[]{
    return this._books;
  }

  @Log("Book added")
  addBook(_books: T): void {
    this._books.push(_books);
  }

  deleteBook(id:number){
    const bookIndex = this._books.findIndex(book => book.id === id);

    if (bookIndex !== -1) {
      this._books.splice(bookIndex, 1);
    }
  }

}

const uni1 = new Univeristy("VDPU");
console.log("We created univeristy:", uni1.name);

const lib1 = new Librarian("George",21);

uni1.addEmployee(lib1);

const book1: Book = { id: 1, subject: Subject.Law, name:Textbook.LawTextbook };

uni1.addBook(book1);

console.log("University book base:", uni1.showBooks());

uni1.deleteBook(1);

console.log("University book base:", uni1.showBooks());

const bookOne: Book = { id: 1, name: Textbook.MathTextbook, subject: Subject.Math};

const bookCard = new ReaderTicket();

bookCard.addItem({ ...bookOne});

const reader = new Reader("name", "gender", 23, bookCard);
const newTeacher = new Teacher("valerii", 20 );

const isSmart = newTeacher.isItBookwarm(reader, uni1);

if (isSmart as string){
  console.log("It is real book worm");
} else{
  console.log("This student has read too few books for a good grade");
}


function Log(message: string): MethodDecorator {
  return function (target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor): void {
      const originalMethod = descriptor.value;

      descriptor.value = function (...args: any[]): any {
          console.log(`${message}: ${JSON.stringify(args)}`);
          return originalMethod.apply(this, args);
      };
  };
};

export{}

